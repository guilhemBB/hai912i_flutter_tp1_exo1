import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FirstCard',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomeCardPage(title: 'FirstCard'),
    );
  }
}

class MyHomeCardPage extends StatefulWidget {
  const MyHomeCardPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomeCardPage> createState() => _MyHomeCardPageState();
}

class _MyHomeCardPageState extends State<MyHomeCardPage> {
  static const IconData person = IconData(0xe491, fontFamily: 'MaterialIcons');

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Stack(
          clipBehavior: Clip.none,
          children: <Widget>[
            Container(
              width: 300,
              height: 200,
              color: Colors.pink,
              child: Center(
                  child: Column(
                    children: [
                      Container(
                        width: 0,
                        height: 70,
                      ),
                      Container(
                        child: Text("Jhon Does",
                            textAlign: TextAlign.center,
                           ),
                      ),
                      Container(
                        child: Text("jhon.does@exemple.com",
                            textAlign: TextAlign.center,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.person),
                          Container(
                            child: Text("twitter: twitter.com/jhonDoes",
                                textAlign: TextAlign.center,
                              ),
                          ),
                        ],
                      )
                    ],
                  )),
            ),
            Positioned(
              bottom: 150,
              right: 70,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage('https://picsum.photos/250?image=9'),
                      fit: BoxFit.fill,
                    ),
                    border: Border.all(
                      color: Colors.pink,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(180))),
                width: 150,
                height: 150,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
